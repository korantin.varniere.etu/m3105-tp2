package fr.univlille.iutinfo.m3105.modelQ1;

import fr.univlille.iutinfo.m3105.utils.Subject;

public class Thermogeekostat extends Subject implements ITemperature {
	
	private double temperature;

	@Override
	public void setTemperature(double d) {
		this.temperature = d;
		this.notifyObservers(getTemperature());
	}

	@Override
	public Double getTemperature() {
		return this.temperature;
	}

	@Override
	public void incrementTemperature() {
		this.temperature++;
		this.notifyObservers(getTemperature());
	}

	@Override
	public void decrementTemperature() {
		this.temperature--;
		this.notifyObservers(getTemperature());
	}

}
