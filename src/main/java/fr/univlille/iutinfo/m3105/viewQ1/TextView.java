package fr.univlille.iutinfo.m3105.viewQ1;

import fr.univlille.iutinfo.m3105.modelQ1.Thermogeekostat;
import fr.univlille.iutinfo.m3105.utils.Observer;
import fr.univlille.iutinfo.m3105.utils.Subject;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TextView extends Stage implements ITemperatureView, Observer {
	
	private Thermogeekostat model;
	private TextArea display;
	private VBox root;
	private HBox temperatureDisplay;
	private Button less, more, lessFive, moreFive;

	public TextView(Thermogeekostat model) {
		this.model = model;
		this.model.attach(this);
		
		this.lessFive = new Button("-5");
		this.lessFive.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				decrementAction();
				decrementAction();
				decrementAction();
				decrementAction();
				decrementAction();
			}
			
		});
		this.moreFive = new Button("+5");
		this.moreFive.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				incrementAction();
				incrementAction();
				incrementAction();
				incrementAction();
				incrementAction();
			}
			
		});
		this.less = new Button("-1");
		this.less.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				decrementAction();
			}
			
		});
		this.more = new Button("+1");
		this.more.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				incrementAction();
			}
			
		});
		
		this.setTitle("Thermogeekostat");
		this.temperatureDisplay = new HBox(5);
		
		this.display = new TextArea(model.getTemperature() + "");
		this.display.setMaxSize(200, this.less.getHeight());
		
		this.root = new VBox(5);
		this.root.setAlignment(Pos.CENTER);
		this.temperatureDisplay.getChildren().addAll(this.lessFive, this.less, this.display, this.more, this.moreFive);
		this.root.getChildren().add(new Label("Température en Celsius"));
		this.root.getChildren().add(this.temperatureDisplay);
		
		this.setScene(new Scene(this.root));
	}

	@Override
	public double getDisplayedValue() {
		return model.getTemperature();
	}

	@Override
	public void incrementAction() {
		model.incrementTemperature();

		display.setText(model.getTemperature() + "");
	}

	@Override
	public void decrementAction() {
		model.decrementTemperature();

		display.setText(model.getTemperature() + "");
	}

	@Override
	public void update(Subject subj) {
	}

	@Override
	public void update(Subject subj, Object data) {
		this.display.setText((Double) (data) + "");
	}
	
}
