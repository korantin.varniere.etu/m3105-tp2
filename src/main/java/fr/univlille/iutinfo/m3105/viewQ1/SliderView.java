package fr.univlille.iutinfo.m3105.viewQ1;

import fr.univlille.iutinfo.m3105.modelQ1.Thermogeekostat;
import fr.univlille.iutinfo.m3105.utils.Observer;
import fr.univlille.iutinfo.m3105.utils.Subject;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SliderView extends Stage implements ITemperatureView, Observer {

	private Thermogeekostat model;
	private VBox root;
	private Slider slide;
	private Button less, more;
	
	public SliderView(Thermogeekostat model) {
		this.model = model;
		this.model.attach(this);
		this.root = new VBox(5);
		this.slide = new Slider(-10, 50, this.model.getTemperature());
		this.less = new Button("-1");
		this.less.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				decrementAction();
			}
			
		});
		this.more = new Button("+1");
		this.more.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				incrementAction();
			}
			
		});
		
		this.slide.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				model.setTemperature(newValue.intValue());
			}
			
		});
		
		this.root.getChildren().addAll(this.less, this.slide, this.more);
		
		this.setScene(new Scene(this.root));
	}
	
	@Override
	public double getDisplayedValue() {
		return this.model.getTemperature();
	}

	@Override
	public void incrementAction() {
		this.model.incrementTemperature();
		
		this.slide.setValue(this.model.getTemperature());
	}

	@Override
	public void decrementAction() {
		this.model.decrementTemperature();

		this.slide.setValue(this.model.getTemperature());
	}

	@Override
	public void update(Subject subj) {
	}

	@Override
	public void update(Subject subj, Object data) {
		this.slide.setValue((Double) data);
	}

}
